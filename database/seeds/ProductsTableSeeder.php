<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name' => 'Iphone',
                'company' => 'Apple',
                'price' =>  999,
                'stock' => 100
            ],
            [
                'name' => 'Galaxy',
                'company' => 'Samsung',
                'price' =>  899,
                'stock' => 100
            ]
        ];

        Product::insert($products);
    }
}
