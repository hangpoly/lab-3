<html>
  <head>
    <title>@yield('title')</title>
    <style>
      .table{
        border-collapse: collapse;
        border: 1px solid black;
      }

      .table th, td {
        border: 1px solid black;
        padding: 8px;
      }
    </style>
  </head>
  <body>
    @section('sidebar')
      This is the Master sidebar
    @show
    <div class="container">
      @yield('content')
    </div>
  </body>
</html>