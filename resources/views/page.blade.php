@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
  <p>This is appended  the master sidebar</p>
@endsection

@section('content')
  <h1>{{ $name }}</h1>
  <p>this is my body content</p>

  <h2>If Statement</h2>
  @if ($day == 'Friday')
    <p>Time to party</p>
  @else
    <p>Time to make money</p>
  @endif

  <h2>Foreach Loop</h2>
  @foreach($drinks as $drink)
    {{ $drink }}<br>
  @endforeach

  <h2>Execute PHP function</h2>
  <p> {{ date('D M, Y')}}</p>
@endsection