@extends('layouts.master')

@section('content')
<h1>Create Product</h1>

<form action="/product" method="POST">
  @method('POST')
  @csrf
  <div>
    <label for="#name">Name</label><br>
    <input  id="name" name="name" type="text">
  </div>
  <div>
    <label for="#company">Company</label><br>
    <select name="company">
      <option value="Apple">Apple</option>
      <option value="Samsung">Samsung</option>
    </select>
  </div>
  <div>
    <label for="#price">Price</label><br>
    <input  id="price" name="price" min="1" max="99999" type="number">
  </div>
  <div>
    <label for="#stock">Stock</label><br>
    <input  id="stock" name="stock" min="1" max="99999" type="number">
  </div>
  <br>
  <button type="submit">Create</button>
</form>

@endsection