@extends('layouts.master')

@section('content')
<h1>Edit Product</h1>
<form action="/product/{{$product->id}}" method="POST">
  @method('PUT')
  @csrf
  <div>
    <label for="#name">Name</label><br>
    <input  id="name" name="name" type="text" value="{{ $product->name }}">
  </div>
  <div>
    <label for="#company">Company</label><br>
    <select name="company">
      <option value="Apple" {{ $product->company == 'Apple' ? 'selected' : '' }}>Apple</option>
      <option value="Samsung" {{ $product->company == 'Samsung' ? 'selected' : '' }}>Samsung</option>
    </select>
  </div>
  <div>
    <label for="#price">Price</label><br>
    <input  id="price" name="price" type="number" min="1" max="99999" value="{{$product->price}}">
  </div>
  <div>
    <label for="#stock">Stock</label><br>
    <input  id="stock" name="stock" type="number" min="1" max="99999" value="{{$product->stock}}">
  </div>
  <button type="submit">Update</button>
  <button type="button" onclick="location.href='/product'">Back</button>
</form>

@endsection