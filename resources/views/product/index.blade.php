@extends('layouts.master')

@section('content')
<h1>Product</h1>
<button onclick="location.href='product/create'">New Product</button>
<br>
<table class="table">
  <tr>
    <th colspan="7">Procduct</th>
  </tr>
  <tr>
    <th>Name</th>
    <th>Company</th>
    <th>Price</th>
    <th>stock</th>
    <th colspan="2">Action</th>
  </tr>
  @foreach ($products as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td>{{ $item->company }}</td>
      <td>{{ $item->price }}</td>
      <td>{{ $item->stock }}</td>
      <td>
        <button onclick="location.href='product/{{$item->id}}/edit'">Edit</button>
      </td>
      <td>
        <form action="product/{{$item->id}}" method="POST">
          @method('DELETE')
          @csrf
          <input hidden name="id" value="{{$item->id }} ">
          <button>Delete</button>
        </form>
      </td>
    </tr>
  @endforeach
</table>
@endsection